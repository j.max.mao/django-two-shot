from receipts.views import (
    ReceiptListView,
    AccountCreateView,
    ExpenseCategoryCreateView,
    ReceiptCreateView,
    ExpenseCategoryListView,
    AccountListView,
)
from django.urls import path

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("accounts/", AccountListView.as_view(), name="account_listview"),
    path(
        "categories",
        ExpenseCategoryListView.as_view(),
        name="category_listview",
    ),
    path(
        "categories/new/",
        ExpenseCategoryCreateView.as_view(),
        name="category_createview",
    ),
    path(
        "account/new/",
        AccountCreateView.as_view(),
        name="account_createview",
    ),
    path(
        "receipts/new",
        ReceiptCreateView.as_view(),
        name="receipts_createview",
    ),
]
