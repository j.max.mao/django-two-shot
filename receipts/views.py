from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView
from receipts.models import Receipt, ExpenseCategory, Account
from django.views.generic.list import ListView

# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "expense/list.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "account/list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "expense/new.html"
    fields = ["name"]

    def form_valid(self, form):
        owner = form.save(commit=False)
        owner.user = self.request.user
        owner.save()
        return redirect("home")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "account/new.html"
    fields = ["name", "number"]

    def form_valid(self, form):
        owner = form.save(commit=False)
        owner.user = self.request.user
        owner.save()
        return redirect("home")


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/new.html"
    fields = [
        "vendor",
        "total",
        "tax",
        "purchaser",
        "category",
        "account",
    ]

    def form_valid(self, form):
        owner = form.save(commit=False)
        owner.user = self.request.user
        owner.save()
        return redirect("home")
